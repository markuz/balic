# -*- coding: utf-8 -*-
from balic import Balic


project = 'Balic'
copyright = '2020, Marek Kuziel'
author = 'Marek Kuziel'
version = Balic.__version__
release = Balic.__version__
extensions = ["sphinx.ext.autodoc"]
source_suffix = '.rst'
master_doc = 'index'
language = None
exclude_patterns = []
pygments_style = "bw"
html_title = "Balic Documentation"
html_short_title = "Balic"
import guzzle_sphinx_theme
html_translator_class = 'guzzle_sphinx_theme.HTMLTranslator'
html_theme_path = guzzle_sphinx_theme.html_theme_path()
html_theme = 'guzzle_sphinx_theme'
html_sidebars = {
    '**': ['logo-text.html', 'globaltoc.html', 'searchbox.html']
}
extensions.append("guzzle_sphinx_theme")
html_theme_options = {
    "project_nav_name": "Balic",
    "projectlink": "https://gitlab.com/markuz/balic",
}
htmlhelp_basename = 'BalicDocs'
html_show_sourcelink = False
html_show_sphinx = True
man_pages = [
    (master_doc, 'balic', 'Balic Documentation',
     [author], 1)
]
