Development
===========

To develop ``balic`` via ``pyenv`` + ``virtualenv``, run::

    pyenv virtualenv 3.8.5 balic                 # create new virtualenv
    git clone https://gitlab.com/markuz/balic    # clone balic repo
    cd balic                                     # get into the repo dir
    pyenv local balic                            # set balic as local
    source ~/.pyenv/versions/balic/bin/activate  # activate new env
    make update                                  # install dependencies
    make develop                                 # build development egg
    make docs                                    # generate documentation
    make test                                    # run tests
