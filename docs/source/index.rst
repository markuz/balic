Balic
=====

    Command-line Linux Containers Toolset

Balic builds and deploys linux containers.

.. toctree::
   :maxdepth: 1

   README
   development
   api
   CHANGELOG


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
