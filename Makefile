.PHONY: help update develop sdist upload_test upload clean docs push test coverage site

help:
	@echo "help        - this help"
	@echo "update      - pip install -U -r requirements.txt"
	@echo "develop     - balic"
	@echo "sdist       - package"
	@echo "upload_test - package + upload release to test pypi"
	@echo "upload      - package + upload release to pypi"
	@echo "clean       - remove build artifacts"
	@echo "docs        - generate HTML documentation"
	@echo "push        - push to git"
	@echo "test        - run tests with coverage report"
	@echo "coverage    - uploads coverage report to coverage.io"
	@echo "site        - publish generated docs to production"

update:
	@pip install -U -r requirements/development.txt

develop:
	python setup.py develop

sdist:
	python setup.py sdist bdist_wheel

upload_test:
	twine upload --sign --identity D048304B --repository-url https://test.pypi.org/legacy/ dist/*

upload:
	twine upload --sign --identity D048304B dist/*

clean:
	rm -rf dist build *.egg-info
	cd docs && make clean

docs:
	cd docs && rm -rf build && make html

push:
	git push origin master
	git push --tags origin master

test:
	@clear
	@pytest --cov=balic tests
	@coverage report -m
	@coverage xml

coverage:
	@clear
	@curl -s https://codecov.io/bash > codecovio.sh
	bash codecovio.sh -t @.cc_token

site:
	@clear
	@cd docs/build && tar cvfz html.tgz html && scp html.tgz balic:/home/balic/html.tgz
	@ssh balic "cd /home/balic && tar xvfz html.tgz && rm -rf www && mv html www"
	@rm docs/build/html.tgz
